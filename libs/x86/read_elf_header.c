#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "elf_header.h"
#include "kernel_func.h"

e_header*
read_elf_header(FILE * file)
{
    e_header * ret_header = (e_header*)malloc(sizeof(e_header));
    for(int i = 0; i < 16; i++){
        ret_header->e_ident[i] = getc(file);
    }
    for(int i = 0; i < 2; i++)
    {
        ret_header->e_type << 8;
        ret_header->e_type += getc(file);
    }
    for(int i = 0; i < 2; i++)
    {
        ret_header->e_machine <<= 8;
        ret_header->e_machine += getc(file);
    }
    for(int i = 0; i < 4; i++)
    {
        ret_header->e_version <<= 8;
        ret_header->e_version += getc(file);
    }
    for(int i = 0; i <  4 * ret_header->e_ident[4]; i++)
    {
        ret_header->e_entry <<= 8;
        ret_header->e_entry += getc(file);
    }
    for(int i = 0; i < 4 * ret_header->e_ident[4]; i++)
    {
        ret_header->e_phoff <<= 8;
        ret_header->e_phoff += getc(file);
    }
    for(int i = 0; i <  4 * ret_header->e_ident[4]; i++)
    {
        ret_header->e_shoff <<= 8;
        ret_header->e_shoff += getc(file);
    }
    for(int i = 0; i < sizeof(ret_header->e_flags); i++)
    {
        ret_header->e_flags <<= 8;
        ret_header->e_flags += getc(file);
    }
    for(int i = 0; i < sizeof(ret_header->e_ehsize); i++)
    {
        ret_header->e_ehsize <<= 8;
        ret_header->e_ehsize += getc(file);
    }
    for(int i = 0; i < sizeof(ret_header->e_phentsize); i++)
    {
        ret_header->e_phentsize <<= 8;
        ret_header->e_phentsize += getc(file);
    }
    for(int i = 0; i < sizeof(ret_header->e_phnum); i++)
    {
        ret_header->e_phnum <<= 8;
        ret_header->e_phnum += getc(file);
    }
    for(int i = 0; i < sizeof(ret_header->e_shentsize); i++)
    {
        ret_header->e_shentsize <<= 8;
        ret_header->e_shentsize += getc(file);
    }
    for(int i = 0; i < sizeof(ret_header->e_shnum); i++)
    {
        ret_header->e_shnum <<= 8;
        ret_header->e_shnum += getc(file);
    }
    for(int i = 0; i < sizeof(ret_header->e_shstrndx); i++)
    {
        ret_header->e_shstrndx <<= 8;
        ret_header->e_shstrndx += getc(file);
    }
    if(need_change(ret_header->e_ident[5])){
        ret_header->e_type = change_endian(ret_header->e_type,sizeof(ret_header->e_type));
        ret_header->e_machine = change_endian(ret_header->e_machine, sizeof(ret_header->e_machine));
        ret_header->e_version = change_endian(ret_header->e_version, sizeof(ret_header->e_version));
        ret_header->e_entry = change_endian(ret_header->e_entry, 4 * ret_header->e_ident[4]);
        ret_header->e_phoff = change_endian(ret_header->e_phoff, 4 * ret_header->e_ident[4]);
        ret_header->e_shoff = change_endian(ret_header->e_shoff, 4 * ret_header->e_ident[4]);
        ret_header->e_flags = change_endian(ret_header->e_flags, sizeof(ret_header->e_flags));
        ret_header->e_ehsize = change_endian(ret_header->e_ehsize, sizeof(ret_header->e_ehsize));
        ret_header->e_phentsize = change_endian(ret_header->e_phentsize, sizeof(ret_header->e_phentsize));
        ret_header->e_phnum = change_endian(ret_header->e_phnum, sizeof(ret_header->e_phnum));
        ret_header->e_shentsize = change_endian(ret_header->e_shentsize, sizeof(ret_header->e_shentsize));
        ret_header->e_shnum = change_endian(ret_header->e_shnum, sizeof(ret_header->e_shnum));
        ret_header->e_shstrndx = change_endian(ret_header->e_shstrndx, sizeof(ret_header->e_shentsize));
    }
        printf("%x\n",ret_header->e_machine);
    return ret_header;
}

e_pheader*
read_elf_pheader(FILE* file,
                 int arch,
                 uint8_t endian)
{
    e_pheader *ret_header = (e_pheader*)malloc(sizeof(e_pheader));

    for(int i = 0; i < sizeof(ret_header->p_type); i++){
        ret_header->p_type <<= 8;
        ret_header->p_type += getc(file);
    }
    if(arch == ELFCLASS64){
      for(int i = 0; i < sizeof(ret_header->p_flags); i++){
        ret_header->p_flags <<= 8;
        ret_header->p_flags += getc(file);
        }
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->p_offset <<= 8;
        ret_header->p_offset += getc(file);
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->p_vaddr <<= 8;
        ret_header->p_vaddr += getc(file);
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->p_paddr <<= 8;
        ret_header->p_paddr += getc(file);
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->p_filesz <<= 8;
        ret_header->p_filesz += getc(file);
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->p_memsz <<= 8;
        ret_header->p_memsz += getc(file);
    }
    if(arch == ELFCLASS32){
        for(int i = 0; i < sizeof(ret_header->p_flags); i++){
            ret_header->p_align <<= 8;
            ret_header->p_align += getc(file);
        }
    }
    if(need_change(endian)){
        ret_header->p_type = change_endian(ret_header->p_type,sizeof(ret_header->p_type));
        ret_header->p_flags = change_endian(ret_header->p_flags, sizeof(ret_header->p_flags));
        ret_header->p_offset = change_endian(ret_header->p_offset, 4 * arch);
        ret_header->p_vaddr = change_endian(ret_header->p_vaddr, 4 * arch);
        ret_header->p_paddr = change_endian(ret_header->p_paddr, 4 * arch);
        ret_header->p_filesz = change_endian(ret_header->p_filesz, 4 * arch);
        ret_header->p_memsz = change_endian(ret_header->p_memsz, 4 * arch);
    }
    printf("%x\n",ret_header->p_flags);
    return ret_header;
}

e_sheader* 
read_elf_sheader(FILE* file,
                 int arch,
                 uint8_t endian)
{
    e_sheader *ret_header = (e_sheader*)malloc(sizeof(e_sheader));
    for(int i = 0; i < sizeof(ret_header->sh_name);i++){
            ret_header->sh_name <<= 8;
            ret_header->sh_name += getc(file);
    }
    for(int i = 0; i < sizeof(ret_header->sh_type);i++){
            ret_header->sh_type <<= 8;
            ret_header->sh_type += getc(file);
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->sh_flags <<= 8;
        ret_header->sh_flags += getc(file);
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->sh_addr <<= 8;
        ret_header->sh_addr += getc(file);
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->sh_offset <<= 8;
        ret_header->sh_offset += getc(file);
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->sh_size <<= 8;
        ret_header->sh_size += getc(file);
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->sh_link <<= 8;
        ret_header->sh_link += getc(file);
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->sh_addralign <<= 8;
        ret_header->sh_addralign += getc(file);
    }
    for(int i = 0; i < 4 * arch; i++){
        ret_header->sh_entsize <<= 8;
        ret_header->sh_entsize += getc(file);
    }
    if(need_change(endian)){
        ret_header->sh_name = change_endian(ret_header->sh_name, sizeof(ret_header->sh_name));
        ret_header->sh_type = change_endian(ret_header->sh_type, sizeof(ret_header->sh_type));
        ret_header->sh_flags = change_endian(ret_header->sh_flags, 4 * arch);
        ret_header->sh_addr = change_endian(ret_header->sh_addr, 4 * arch);
        ret_header->sh_offset = change_endian(ret_header->sh_offset, 4 * arch);
        ret_header->sh_size = change_endian(ret_header->sh_size, 4 * arch);
        ret_header->sh_link = change_endian(ret_header->sh_link, 4);
        ret_header->sh_addralign = change_endian(ret_header->sh_addralign, 4 * arch);
        ret_header->sh_entsize = change_endian(ret_header->sh_entsize, 4 * arch);
    }  
    printf("%x\n",ret_header->sh_flags);
    return ret_header;
}


