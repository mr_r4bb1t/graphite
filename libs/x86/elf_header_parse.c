#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "kernel_func.h"
#include "elf_header.h"

char* parse_elf_class(uint8_t elf_class){
    switch (elf_class)
    {
        case ELFCLASSNONE:
            return "Некорректный класс";
        case ELFCLASS32:
            return "32-битный объектный файл";
        case ELFCLASS64:
            return "64-битный объектный файл";
        default:
            break;
    }
}

char* parse_elf_data(uint8_t elf_data){

    switch(elf_data){
        case ELFDATANONE:
            return "Некорректный тип";
        case ELFDATA2LSB:
            return "От младшего к старшему";
        case ELFDATA2MSB:
            return "От старшего к младшему";
        default:
            break;
    }

}

void parse_elf_osabi(uint8_t elf_osabi){
    
}