# configuration parameters
COMPILE = gcc
MAIN_DIR = kernel
COMPILE_FLAGS = -c -I ./include
RESULT_PROGRAM = graphite
JSON = json-c
ZYDIS = Zydis

all:
	$(COMPILE) $(MAIN_DIR)/main.c $(COMPILE_FLAGS) 
	$(COMPILE) $(MAIN_DIR)/change_endian.c $(COMPILE_FLAGS) 
	$(COMPILE) $(MAIN_DIR)/get_file_type.c $(COMPILE_FLAGS) 
	$(COMPILE) $(MAIN_DIR)/need_change.c $(COMPILE_FLAGS) 
	$(COMPILE) $(MAIN_DIR)/research_elf_file.c $(COMPILE_FLAGS) 
	$(COMPILE) $(MAIN_DIR)/anal/anal_terminal.c $(COMPILE_FLAGS) 
	$(COMPILE) commands/print_bin_file.c $(COMPILE_FLAGS) 
	$(COMPILE) commands/help.c $(COMPILE_FLAGS)
	$(COMPILE) commands/search_signature.c $(COMPILE_FLAGS) 
	$(COMPILE) commands/commands.c $(COMPILE_FLAGS) 
	$(COMPILE) commands/jump_to.c $(COMPILE_FLAGS) 
	$(COMPILE) commands/set_file.c $(COMPILE_FLAGS)
	$(COMPILE) libs/x86/read_elf_header.c $(COMPILE_FLAGS) 
	$(COMPILE) libs/x86/elf_header_parse.c $(COMPILE_FLAGS) 
	$(COMPILE) *.o -o $(RESULT_PROGRAM) -lpthread
	
clear:
	rm *.o