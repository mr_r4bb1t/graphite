#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <termios.h>
#include <ctype.h>
#include <pthread.h>

#include "anal.h"
#include "commands.h"

#define COLLUMN_COUNT 16
#define WRONG_COMMAND 0xff


void
print_bin_file(char* filename)
{
    char search_mode = 1;
    FILE * file = fopen(filename,"r+b");
    uint8_t c;
    uint64_t addr = 0;
    struct winsize w;
    uint64_t step = 0;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    uint64_t low_addr = 0;
    uint64_t row_count = w.ws_row - 2;
    uint64_t row = 0;
    uint64_t coll = 0;
    char key;
    while(1){
        system("clear");
        uint64_t find_sig_step = 1;
        uint8_t command_arr[row_count][COLLUMN_COUNT];
        char hex_arr[row_count][COLLUMN_COUNT];
        uint8_t shift[COLLUMN_COUNT];
        for(int i = 0; i < COLLUMN_COUNT; i++)
            shift[i] = i;
        low_addr = addr;
        uint8_t c;
        int64_t jump_addr = get_jump_addr();
        if(jump_addr != -1){
            fseek(file,jump_addr,SEEK_SET);
            addr = jump_addr;
            row = 0;
            coll = 0;
            step = jump_addr / COLLUMN_COUNT;
        }
        for(int i = 0; i< row_count;i++){
            for(int j = 0; j < COLLUMN_COUNT; j++){
                if(c != EOF){
                    c = getc(file);
                    command_arr[i][j] = c;  
                    if(isalpha(c) || isdigit(c))
                        hex_arr[i][j] = (char)c;
                    else
                        hex_arr[i][j] = '.';   
                }
                else
                    command_arr[i][j] = WRONG_COMMAND;
                    hex_arr[i][j] = WRONG_COMMAND;
            }
        }
        printf("      ");
        for(int i = 0; i < COLLUMN_COUNT; i++)
            printf("%.2x ",shift[i]);
        printf("  ");
        for(int i = 0; i < COLLUMN_COUNT; i++)
            printf("%x ",shift[i] % 16);
        printf("\n");
        for(int i = 0; i < row_count; i++){
            printf("\033[0m%4x> ",addr);           
            addr += COLLUMN_COUNT;
            
            for(int j = 0; j < COLLUMN_COUNT;j++){
                if(i == row && j == coll){
                    printf("\033[0;31m%.2x ",command_arr[i][j]);
                }
                else{
                    printf("\033[0m%.2x ",command_arr[i][j]);     
                }
            }
            printf("  ");
            for(int j = 0; j < COLLUMN_COUNT;j++){
                char t;
                if(isalpha(command_arr[i][j]) || isdigit(command_arr[i][j]))
                    t = command_arr[i][j];
                else
                    t = '.';
                if(i == row && j == coll){
                    printf("\033[0;31m%c ", t);
                }
                else{
                    printf("\033[0m%c ", t);      
                }
            }
            printf("\n");
        }
        int is_command = 1;
        while(is_command){
        set_keypress();
        c = getchar();
        switch(c){
            case 'h':
                if(coll > 0){
                    coll--;
                }
                fseek(file,step * COLLUMN_COUNT,SEEK_SET);
                addr = low_addr;
                is_command = 0;
                break;
            case 'k':
                if(row > 0){
                    row--;
                    fseek(file,step * COLLUMN_COUNT,SEEK_SET);
                    addr = low_addr;
                    is_command = 0;
                }
                else{
                    if(step > 0){
                        step--;
                        fseek(file, step * COLLUMN_COUNT,SEEK_SET);
                        addr = low_addr-COLLUMN_COUNT;
                        is_command = 0;
                    }
                }
                if(low_addr == 0){
                    addr = 0;
                    fseek(file, 0, SEEK_SET);
                    is_command = 0;
                }
                break;
            case 'j':
                if(row < row_count-1){
                    fseek(file,step * COLLUMN_COUNT,SEEK_SET);
                    row++;
                    addr = low_addr;
                    is_command = 0;
                }
                else{
                    step++;
                    fseek(file, step * COLLUMN_COUNT,SEEK_SET);
                    addr = low_addr+COLLUMN_COUNT;
                    is_command = 0;
                }
                break;
            case 'l':
                if(coll < COLLUMN_COUNT - 1){
                    coll++;
                }
                fseek(file, step * COLLUMN_COUNT,SEEK_SET);
                addr = low_addr;
                is_command = 0;
                break;
            case 's':
                fseek(file,0, SEEK_SET);
                step = 0;
                row = 0;
                coll = 0;
                addr = 0;
                is_command = 0;
                break;
            case 'i':
                reset_keypress();
                printf("Hex value> ");
                uint8_t change_hex_value;
                scanf("%x",&change_hex_value);  
                fseek(file,(step + row) * COLLUMN_COUNT + (coll), SEEK_SET);
                putc(change_hex_value,file);
                fseek(file,step * COLLUMN_COUNT,SEEK_SET);
                addr = low_addr;
                is_command = 0;
                break;
            case 'q':
                reset_keypress();
                system("clear");
                return;
            default:
                break;
            }
        reset_keypress();
        }
    }
}
