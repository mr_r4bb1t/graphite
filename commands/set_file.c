#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>

#include "lists.h"
#include "kernel_func.h"
#include "file_type.h"
#include "anal.h"

char *filepath;

char*
get_filepath(){
    return filepath;
}

void
set_file(){
char **files = (char**)malloc(0);
	uint16_t *files_type = (uint16_t*)malloc(0);
    filepath = (char*)malloc(1);
	DIR * dir;
	struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    int row_count = w.ws_row - 1;
	struct dirent *ent;
	*(filepath) = '.';
	int checked_file = 1;
	int cur_file = 0;
	int change_dir = 1;
	int file_count;
	int step = 0;
	while(checked_file){
		if(change_dir){
			file_count = 0;
			cur_file = 0;
			dir = opendir(filepath);
			while((ent=readdir(dir)) != NULL){
				file_count++;
				printf("\t%s\n",ent->d_name);
				char *arr = (char*)malloc(255);
				strcpy(arr,ent->d_name);
				files = (char**)realloc(files,file_count*255);
				files_type = (uint16_t*)realloc(files_type,file_count * sizeof(uint16_t));
				*(files + file_count - 1) = arr;
				*(files_type + file_count - 1) = ent->d_type;
			}
			change_dir = 0;
		}
		system("clear");
		int i = step;
		while(i < file_count && i < row_count + step){
			if(cur_file == i){
				printf("\033[31m> ");
				printf("\t%s\n",*(files+i));
			}
			else{
				printf("\033[0m\t%s\n",*(files + i));
			}
			i++;
		}
		set_keypress();
		uint8_t c = getchar();
		uint64_t filepath_size = strlen(filepath);
		switch(c){
			case 'j':
				if(cur_file < file_count -1)
					cur_file++;
				if(cur_file > row_count - 1)
					step++;	
				break;
			case 'k':
				if(cur_file > 0)
					cur_file--;
				if(cur_file > step && step > 0)
					step--;
				break;
			case 'h':
			if(filepath_size != 1){
				do{
					filepath_size--;
				}while(*(filepath + filepath_size) != '/' && filepath_size != 1);
				memset(filepath + filepath_size,0, strlen(filepath) - filepath_size);
				filepath = (char*)realloc(filepath,filepath_size);
				change_dir = 1;
			}
				break;
			case 'l':
				filepath = (char*)realloc(filepath,filepath_size + 1);
				*(filepath + filepath_size) = '/';
				filepath = (char*)realloc(filepath, strlen(filepath) + strlen(*(files + cur_file)));
				sprintf(filepath,"%s%s",filepath,*(files + cur_file));
				change_dir = 1;
				if(*(files_type + cur_file) == DT_REG){
					checked_file = 0;
				}
				break;
			case 'q':
				system("clear");
				reset_keypress();
				return;
			default:
				break;
		}
        reset_keypress();
	}
    
}