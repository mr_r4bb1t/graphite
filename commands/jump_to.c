#include <ctype.h>
#include <string.h>
#include <stddef.h>
#include <stdint.h>


int64_t jump_addr = 0;

int64_t
get_jump_addr(){
    int64_t ret_num;
    if(jump_addr == 0)
        ret_num = -1;
    else
        ret_num = jump_addr;
    jump_addr = 0;
    return ret_num;
}

void
jump_to(char * value){
    int64_t ret_value = 0;
    size_t size = strlen(value);
    
        for(int i = 0; i < size; i++){
            ret_value <<= 4;
            if(isdigit(value[i])){
                ret_value += value[i] - '0';
                continue;
            }
            if(isalpha(value[i])){
                ret_value += (value[i] - 'a') + 10; 
                continue;
            }
        }
    
    jump_addr = ret_value;
}