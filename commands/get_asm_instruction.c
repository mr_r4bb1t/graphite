#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <Zydis/Zydis.h>

void
get_asm_instruction(char * value, char *filename){
    uint64_t value[2];
    char * first_value = strtok(value,",");
    char * second_value = strtok(NULL, ",");
    for(int i = 0; i < strlen(first_value);i++){
        value[0] <<= 4;
        if(isdigit(first_value)){
            value[0] += first_value - '0';
            continue;
        }
        if(isalpha(first_value)){
            value[0] += (first_value - 'a') + 10; 
            continue;
        }
    }
    for(int i = 0; i < strlen(second_value);i++){
        value[1] <<= 4;
        if(isdigit(first_value)){
            value[1] += second_value - '0';
            continue;
        }
        if(isalpha(first_value)){
            value[1] += (second_value - 'a') + 10; 
            continue;
        }
    }
    uint64_t min;
    uint64_t max;
    uint64_t size;
    min = value[1] > value[0] ? value[0] : value[1];
    max = value[1] < value[0] ? value[0] : value[1];
    size = max - min + 1;
    FILE *file = fopen(filename, "rb");
    fseek(file, min, SEEK_SET);
    uint8_t hex_values[size];
    for(int i = 0; i < size; i++ ){
        hex_values[i] = getc(file);
    }   
    
}