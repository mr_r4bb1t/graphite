#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>

#include "commands.h"

commands_t *command;

void
set_command_parameter(commands_t *x){
    command = x;
}

void*
commands(){
    char *func_name = strtok(command->func_with_value, "()");
    char *func_values = strtok(NULL,"()");
    char * file = get_filepath();
    if(!strcmp(func_name,"help")){
        help(func_values);
    }
    if(!strcmp(func_name,"search_signature")){
        uint8_t search_sig[strlen(func_values)/ 2];
        memset(search_sig,0, strlen(func_values)/2);
        for(int i = 0; i < strlen(func_values); i++){
            search_sig[i/2] <<= 4;
            if(isdigit(func_values[i])){
                search_sig[i/2] += func_values[i] - '0';
                continue;
            }
            if(isalpha(func_values[i])){
                search_sig[i/2] += (uint8_t)(func_values[i] - 'a') + 10; 
                continue;
            }
        }
        search_signature_t* search = (search_signature_t*)malloc(sizeof(search_signature_t));
        search->search_sig = (char*)malloc(strlen(func_values));
        strcpy(search->search_sig,search_sig);
        search->sig_size = strlen(func_values)/2;
        
        search->filename = (char*)malloc(strlen(file));
        strcpy(search->filename,file);
        set_search_parameter(search);
        search_signature();
    }
    if(!strcmp(func_name,"jump_to"))
    {
        jump_to(func_values);
    }
    if(!strcmp(func_name,"set_file"))
    {
        set_file();
    }
    if(!strcmp(func_name,"get_file"))
    {
        printf("%s\n",get_filepath());
    }
    if(!strcmp(func_name, "print_hex_value"))
    {
        print_bin_file(file);
    }
}