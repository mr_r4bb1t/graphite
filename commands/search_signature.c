#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <pthread.h>

#include "commands.h"

uint64_t *finding_signature = NULL;
int is_find = 0;

uint64_t find_sig_size = 0;

search_signature_t *search;

void
set_search_parameter(search_signature_t *x)
{
    search = x;
}

void
set_find_signature(uint64_t *x)
{
    is_find = 1;
    finding_signature = x;
}

uint64_t *
get_find_signature()
{
    if (is_find) 
        return finding_signature;
    else
        return NULL;
}

uint64_t
get_size_signature()
{
    return find_sig_size;
}

void
search_signature(){
    uint64_t *ret_param =(uint64_t*)malloc(sizeof(uint64_t));
    *(ret_param) = 0;
    find_sig_size = search->sig_size;
    uint64_t size = 0;
    uint8_t search_signature[search->sig_size];
    FILE*fp = fopen(search->filename, "r");
    for(uint64_t i; i < search->sig_size; i++)
        search_signature[i] = (uint8_t)getc(fp);
    uint64_t step = 0;
    uint8_t c;
    char loading[] = {'\\','|','/','-'};
    int loading_step = 0;
    while((c = getc(fp)) != EOF){
        uint8_t is_equal = 1;
        loading_step++;
        loading_step %= 4;
        for(uint64_t i = 0; i < find_sig_size; i++){
            if(search_signature[i] != search->search_sig[i]){
                is_equal = 0;
                break;
            }
        }
        if(is_equal){
            size++;
            ret_param = (uint64_t*)realloc(ret_param, (size + 1) * sizeof(size));
            *(ret_param + size) = step - search->sig_size;
            uint64_t shift = step % 16; 
            uint64_t addr = step - shift;
            printf("addr: %x shift: %x\n", addr, shift);
            *(ret_param) = size;
            set_find_signature(ret_param);
        }
        for(uint64_t i = 0; i < search->sig_size - 1; i++){
            search_signature[i] = search_signature[i + 1];
        }
        search_signature[find_sig_size - 1] = c;
        step++;
    }
}