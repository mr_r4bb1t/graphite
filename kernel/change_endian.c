#include <stdint.h>
#include <stddef.h>

uint64_t
change_endian(uint64_t item,
              size_t size){
    uint64_t x = item;
    uint64_t ret = 0;
    for(size_t i = 0; i < size; i++){
        ret <<= 8;
        ret += x % 256;
        x >>= 8;
    }
    return ret;
}