#include <stdio.h>
#include <stdint.h>

void
read_bin_file(void* there, 
              size_t item_size,
              size_t count,
              FILE * file){
    uint8_t* item = (uint8_t*)malloc(item_size * count);
    for(size_t i = 0; i < item_size * count; i++){
        *(item + i) = getc(file);
        //printf("%x ",*(item + i));
    }
    there = *item;
}