#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include "elf_header.h"
#include "read_elf_header.h"
#include "kernel_func.h"

ELF_HEADERS*
research_elf_file(FILE* file){

    ELF_HEADERS* ret_header = (ELF_HEADERS*)malloc(sizeof(ELF_HEADERS));

    uint16_t pheaders_count;
    uint16_t sheaders_count;

    printf("Reading header\n");
    //Reading elf main header
    e_header *e_head = read_elf_header(file);
    //printf( "%x",e_head->e_ident[0]);
    printf("Reading program header\n");
    pheaders_count = e_head->e_phnum;
    sheaders_count = e_head->e_shnum;

    //Reading elf program header
    printf("%d\n",e_head->e_phnum);
    e_pheader **e_pheaders = (e_pheader**)malloc(e_head->e_phnum * e_head->e_ehsize);
    if(e_head->e_phoff != 0){
        for(int i = 0; i < e_head->e_phnum;i++){
            printf("Program header %d\n", i+1);
            fseek(file, SEEK_SET, e_head->e_phoff + e_head->e_ehsize * i);
            *(e_pheaders + i) = read_elf_pheader(file, e_head->e_ident[4],e_head->e_ident[5]);
	    }
    }
    printf("Reading section table\n");
    //Readign elf section table header
    e_sheader **e_sheaders = (e_sheader**)malloc(e_head->e_shnum * e_head->e_shentsize);
    if(e_head->e_shoff != 0){
        for(int i = 0; i < e_head->e_shnum; i++){
            printf("Section table %d\n",i+1);
            fseek(file, SEEK_SET, e_head->e_shoff + e_head->e_shentsize * i);
            *(e_sheaders + i) = read_elf_sheader(file, e_head->e_ident[4],e_head->e_ident[5]);
        }
    }
    ret_header->e_head = e_head;
    ret_header->p_header_count = pheaders_count;
    ret_header->sh_header_count = sheaders_count;
    ret_header->s_headers = e_sheaders;
    ret_header->p_headers = e_pheaders;

    return ret_header;
}
