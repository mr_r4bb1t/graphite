#include <stdint.h>

void
cmp_array(uint8_t*first_array,
          uint64_t fa_size,
          uint8_t*second_array,
          uint64_t sa_size){

    if(fa_size != sa_size)
        return 1;
    for(uint64_t i = 0; i < fa_size;i++){
        if(*(first_array + i) != *(second_array + i))
            return 1;
    }
    return 0;

}