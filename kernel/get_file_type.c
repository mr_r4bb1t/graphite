#include <stdio.h>
#include <stdint.h>
#include "file_type.h"
#include "sig_file.h"

uint64_t
get_file_type(FILE *file){
    uint32_t signature;
    for(size_t i = 0; i < sizeof(signature); i++){
        signature <<= 8;
        signature += getc(file);
    }
    fseek(file,SEEK_SET,0);
    switch (signature)
    {
    case ELF_SIG:
        return ELF;
    case PE_SIG:
        return PE;
    default:
        return NONE;
    }
}