#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <pthread.h>

#include "anal.h"
#include "commands.h"

#define MODE_COUNT 2

int
main(void){	
	system("clear");
	printf(" ####  #####    ##   #####  #    # # ##### ######\n");
	printf("#    # #    #  #  #  #    # #    # #   #   #     \n");
	printf("#      #    # #    # #    # ###### #   #   ##### \n");
	printf("#  ### #####  ###### #####  #    # #   #   #     \n");
	printf("#    # #   #  #    # #      #    # #   #   #     \n");
	printf(" ####  #    # #    # #      #    # #   #   ######\n");
	while(1){
		char buff[1024];
		memset(buff, 0, 1024);
		pthread_t tid;
		pthread_attr_t attr;
		printf("Enter command: ");
		scanf("%s",buff);
		pthread_attr_init(&attr);
		commands_t *command = (commands_t*)malloc(sizeof(commands_t));
		command->func_with_value =(char*)malloc(strlen(buff));
		command->func_with_value = buff;
		set_command_parameter(command);
		commands();
		//pthread_create(&tid, &attr, commands, NULL);
		//pthread_join(tid,NULL);
		reset_keypress();
		free(command);
	}
}
