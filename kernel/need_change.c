#include <stdbool.h>
#include <stdint.h>
#include "elf_header.h"

bool
need_change(uint16_t machine){
    if(machine == ELFDATA2LSB){
        return true;
    }
    if(machine == ELFDATA2MSB){
        return false;
    }
}