#ifndef KERNEL_FUNC
#define KERNEL_FUNC

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "elf_header.h"

struct list{
    char item[256];
    struct list* next;
};



uint64_t get_file_type(FILE*);
ELF_HEADERS* research_elf_file(FILE*);
uint64_t change_endian(uint64_t,size_t);
bool need_change(uint16_t);
void read_bin_file(void*, size_t, size_t, FILE*);
int8_t cmp_array(uint8_t*, uint64_t, uint8_t*, uint64_t);
void sa_loading();
void print_bin_file(const char*);

#endif