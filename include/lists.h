#ifndef LISTS
#define LISTS

#include <stdint.h>

struct List{
	uint64_t addr;
	char name[256];
	struct List *next;
};

typedef struct{
	char name[32];
	uint64_t first_operand;
	uint64_t second_operand;
	uint64_t third_operand;
}opcodes;

#endif