#ifndef OPCODE_TABLE
#define OPCODE_TABLE

typedef struct{
    char name[32];
    uint64_t opcode;
    char first_operand[32];
    char second_operand[32];
} opcode;

typedef opcode* opcode_t;

#endif