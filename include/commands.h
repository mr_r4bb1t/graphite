#ifndef COMMANDS
#define COMMANDS

#include <stdint.h>

typedef struct{
    uint8_t * search_sig;
    uint64_t sig_size;
    char * filename;

} search_signature_t;

typedef struct{
    char *func_with_value;
    char *filename;
} commands_t;

void help(const char*);
void search_signature();
void set_search_parameter(search_signature_t*);

void* commands();

void set_command_parameter(commands_t*);
uint64_t* get_find_signature();
uint64_t get_size_signature();

void jump_to(char *);
uint64_t get_jump_addr();

void set_file();
char* get_filepath();

void print_bin_file(char*);
#endif