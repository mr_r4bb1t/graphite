#ifndef FILE_SIG
#define FILE_SIG

#define ELF_SIG 0x7f454c46
#define PE_SIG  0x50450000
#define EXE_SIG 0x4d5a0000
#define JAR_SIG 0x504b0304
#define JAR_EMPTY_SIG 0x504b0506
#define JAR_SPANNED_SIG 0x504b0708

#endif