#ifndef READ_ELF_HEADER
#define READ_ELF_HEADER
#include <stdio.h>
#include <stdint.h>
#include "elf_header.h"

e_header *read_elf_header(FILE*);
e_pheader *read_elf_pheader(FILE*, int,uint8_t);
e_sheader *read_elf_sheader(FILE*, int,uint8_t);
#endif